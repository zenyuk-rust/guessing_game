use std::cmp::Ordering;
use std::io;
use rand::Rng;

fn main() {
    loop {
        println!("Waiting for input ...");
        let mut input: String = String::new();
        io::stdin().read_line(&mut input).expect("Error reading user input");
        println!("Your input was: {}", input);
        let numeric_input: u32 = match input.trim().parse() {
            Ok(num) => num,
            Err(_) => continue,
        };

        let my_random = rand::thread_rng().gen_range(0..100);
        println!("Random: {}", my_random);

        match numeric_input.cmp(&my_random) {
            Ordering::Less => println!("less"),
            Ordering::Equal => {
                println!("You win!");
                break;
            },
            Ordering::Greater => println!("greater"),
        }
    }
}
